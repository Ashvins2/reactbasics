import React, { useState, isValidElement } from 'react';
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import TextBoxFormik from "./components/FormikFields/TextBoxFormik"
import FormikSelect,{FormikSelectItem} from "./components/FormikSelect/FormikSelect"
import MatarialFormikSelect,{MatarialFormikSelectItem} from "./components/FormikSelect/MatarialFormikSelect"
interface FormValues {
    name: string,
    lastName: string,
    age: number,
    position: string,
    email:string,
}

const SignupSchema = Yup.object().shape({
    name: Yup.string().required('Can be emprty'),
    lastName: Yup.string().required('Can be emprty'),
    age: Yup.number().required('Can be emprty!!').min(18, 'your are not allowed!!'),
    position: Yup.string().required('Can be emprty!!'),
    email:Yup.string().required('can\'t be empty!!').email('please give a vaild email!!')

});

const initialValues: FormValues = {
    name: '',
    age: 0,
    lastName: '',
    position: '',
    email:'',
}

const positionItems: FormikSelectItem[] =[
    {
    label:'Front End',
    value:'front_end'
   },{
    label:'Back End',
    value:'back_end'
   },{
    label:'Dev ops',
    value:'dev_ops'
   },{
    label:'Qa',
    value:'qa'
   }
]

const App: React.FC = () => {

    const handleOnSubmit = (values: FormValues): void => {
        alert(JSON.stringify(values));
    }

    return (
        <div className="App">
            <h1> Sign up </h1>
            <Formik initialValues={initialValues}
                onSubmit={handleOnSubmit}
                validationSchema={SignupSchema}
            >
                {porps => {
                    return (
                        <Form>
                            <TextBoxFormik label = {'Your Name'} name = {"name"} type={"text"} required/>
                            <TextBoxFormik label = {'Your Last Name'} name = {"lastName"} type={"text"}/>
                            <TextBoxFormik label = {'Your age'} name = {"age"} type={"number"} required/>
                            <TextBoxFormik label = {'Your email'} name = {"email"} type={"text"} required/>
                            {/*<FormikSelect label = {'Your Position'} name = {"position"} type={"text"} items={positionItems}/>*/ }
                            <MatarialFormikSelect label = {'Your Matarial Position'} name = {"position"} type={"text"} items={positionItems} required/>
                            <button disabled={(!porps.dirty || !porps.isValid)} type='submit'>Submit</button>
                        </Form>

                    );
                }}
            </Formik>
        </div>
    );
};

export default App;