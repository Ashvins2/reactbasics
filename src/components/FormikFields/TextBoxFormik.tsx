import React from 'react'
import './TextBoxFormik.css'
import { Formik, Field, ErrorMessage } from 'formik'
import { TextField } from '@material-ui/core';

interface TextBoxFormikProps {
    label: string,
    name: string,
    type?: string,
    required?:boolean,

}

const TextBoxFormik: React.FC<TextBoxFormikProps> = ({ label, name, type = "text",required=false }) => {
    return <div className="FormikField">

        <Field
            autoComplete="off" required={required} name={name} fullWidth label={label} type={type}
            helperText={<ErrorMessage name={name} />}

            as={TextField} />
        { /* <TextField autoComplete="off" required name={name} fullWidth label={label} type={type} />*/
        }
    </div>
}

export default TextBoxFormik;