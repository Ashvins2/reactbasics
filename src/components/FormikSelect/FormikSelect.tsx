import React from 'react'
import './FormikSelect.css'
import { InputLabel, MenuItem, FormHelperText, FormControl, Select } from '@material-ui/core';

export interface FormikSelectItem {
    label: string,
    value: string,
}

interface FormikSelectprops {
    items: FormikSelectItem[],
    label: string,
    name: string,
    type?: string,
}

const FormikSelect: React.FC<FormikSelectprops> = ({ items, label, name, type = "text" }) => {
    return (
        <div className="FormikSelect">
            <FormControl fullWidth>
                <InputLabel>{label}</InputLabel>
                <Select>
                    {items.map(item => (<MenuItem key={item.value} value={item.value}>{item.label}</MenuItem>))}
                </Select>
                <FormHelperText>Truth Testify!!!</FormHelperText>
            </FormControl>

        </div>

    )
}

export default FormikSelect;