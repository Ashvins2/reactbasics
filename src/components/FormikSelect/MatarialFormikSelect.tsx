import React, { ReactNode } from 'react'
import { Formik, Field, ErrorMessage, FieldInputProps } from 'formik'
import './FormikSelect.css'
import { InputLabel, MenuItem, FormHelperText, FormControl, Select } from '@material-ui/core';

export interface MatarialFormikSelectItem {
    label: string,
    value: string,
}

interface MatarialFormikSelectprops {
    items: MatarialFormikSelectItem[],
    label: string,
    name: string,
    type?: string,
    required?:boolean,
}

interface MatarialUiSelectFieldprops extends FieldInputProps<string> {
    label: string,
    errorString?: string,
    children: ReactNode,
    required:boolean,
}

const MatarialUiSelectField: React.FC<MatarialUiSelectFieldprops> = ({ errorString, label, children, value, name, onChange, onBlur,required }) => {
    return (
        <FormControl fullWidth>
            <InputLabel required={required}>{label}</InputLabel>
            <Select  name={name} onChange={onChange} onBlur={onBlur} value={value} >
                {children}
            </Select> 
        <FormHelperText> {errorString}</FormHelperText>
        </FormControl >
    );
}


const MatarialFormikSelect: React.FC<MatarialFormikSelectprops> = ({ items, label, name, type = "text",required=false }) => {
    return (
        <div className="FormikSelect">
            <Field
                as={MatarialUiSelectField}
                name={name}
                required={required}
                errorString={<ErrorMessage name={name} />}
                label={label}>
                {items.map(item => (<MenuItem key={item.value} value={item.value}>{item.label}</MenuItem>))}
            </Field>

        </div>
    );
}

export default MatarialFormikSelect;